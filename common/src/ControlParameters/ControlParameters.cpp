/*! @file ControlParameters.cpp
 *  @brief Interface to set gains/control parameters for simulator and robot
 *  These are designed to be updated infrequently.  For high frequency data,
 * consider using Driver Inputs or adding to the Robot Debug Data instead.
 */

#include <utility>
#include "ros/ros.h"
#include "common/ControlParameters/ControlParameters.h"
#include "common/Utilities/utilities.h"
#include "common/ControlParameters/ParamHandler.h"


// #define YAML_COLLECTION_NAME_KEY "__collection-name__"

/*!
 * Convert control parameter data types into strings
 */
std::string controlParameterValueKindToString(
    ControlParameterValueKind valueKind) {
  switch (valueKind) {
    case ControlParameterValueKind::S64:
      return "s64";
    case ControlParameterValueKind::DOUBLE:
      return "double";
    case ControlParameterValueKind::FLOAT:
      return "float";
    case ControlParameterValueKind::VEC3_DOUBLE:
      return "vec3d";
    case ControlParameterValueKind::VEC3_FLOAT:
      return "vec3f";
    default:
      return "unknown-ControlParameterValueKind";
  }
}

/*!
 * Convert a control parameter value into a human readable string.
 */
std::string controlParameterValueToString(ControlParameterValue value,
                                          ControlParameterValueKind kind) {
  std::string result;
  switch (kind) {
    case ControlParameterValueKind::DOUBLE:
      result += numberToString(value.d);
      break;
    case ControlParameterValueKind::FLOAT:
      result += numberToString(value.f);
      break;
    case ControlParameterValueKind::S64:
      result += std::to_string(value.i);
      break;
    case ControlParameterValueKind::VEC3_FLOAT:
      result += "[";
      result += numberToString(value.vec3f[0]) + ", ";
      result += numberToString(value.vec3f[1]) + ", ";
      result += numberToString(value.vec3f[2]) + "]";
      break;
    case ControlParameterValueKind::VEC3_DOUBLE:
      result += "[";
      result += numberToString(value.vec3d[0]) + ", ";
      result += numberToString(value.vec3d[1]) + ", ";
      result += numberToString(value.vec3d[2]) + "]";
      break;
    default:
      result += "<unknown type " + std::to_string((u32)(kind)) +
                "> (add it yourself in ControlParameterInterface.h!)";
      break;
  }
  return result;
}

/*!
 * Check if all parameters have had their value set
 * @return if all set
 */
bool ControlParameterCollection::checkIfAllSet() {
  for (auto& kv : _map) {
    if (!kv.second->_set) {
      return false;
    }
  }
  return true;
}

/*!
 * Mark all parameters as not set
 */
void ControlParameterCollection::clearAllSet() {
  for (auto& kv : _map) {
    kv.second->_set = false;
  }
}

/*!
 * Remove all parameters
 */
void ControlParameterCollection::deleteAll() {
  for(auto& kv : _map) {
    delete kv.second;
  }
  _map.clear();
}

/*!
 * Write a list of uninitialized parameters to a string
 */
std::string ControlParameters::generateUnitializedList() {
  std::string result;
  for (auto& kv : collection._map) {
    if (!kv.second->_set) {
      result += kv.second->_name + "    :\n";
    }
  }

  return result;
}

/*!
 * Write all parameters and values to a YAML file
 * @param path : the file name
 */
void ControlParameters::writeToYamlFile(const std::string& path) {
  // writeStringToFile(path, collection.printToYamlString());
}

/*!
 * Determine the kind of a control parameter from a string representation
 * @param str : the string (like "2", or "[2,3,3]")
 * @return the kind
 */
ControlParameterValueKind getControlParameterValueKindFromString(const std::string& str) {
  if(str.find('[') != std::string::npos) {
    // vec type
    if(str.find('f') != std::string::npos) {
      return ControlParameterValueKind::VEC3_FLOAT;
    } else {
      return ControlParameterValueKind::VEC3_DOUBLE;
    }
  } else {
    // scalar type
    if(str.find('.') != std::string::npos) {
      if(str.find('f') != std::string::npos) {
        return ControlParameterValueKind::FLOAT;
      } else {
        return ControlParameterValueKind::DOUBLE;
      }
    } else {
      // integer
      return ControlParameterValueKind::S64;
    }
  }
}

/*!
 * Add and initialize parameters from a YAML file
 */
void ControlParameters::defineAndInitializeFromYamlFile(const std::string &path) {
  ParamHandler paramHandler(path);

  std::vector<std::string> keys = paramHandler.getKeys();

  for (auto& key : keys) {
    std::string valueString;
    paramHandler.getString(key, valueString);
    ControlParameterValueKind kind;
    if(valueString.empty()) {
      kind = ControlParameterValueKind::VEC3_DOUBLE;
    } else {
      kind = ControlParameterValueKind::DOUBLE;
    }

    ControlParameter* cp = new ControlParameter(key, kind);
    collection.addParameter(cp, key);
    switch (cp->_kind) {
      case ControlParameterValueKind::DOUBLE: {
        double d;
        assert(paramHandler.getValue(key, d));
        cp->initializeDouble(d);
      } break;

      case ControlParameterValueKind::FLOAT: {
        float f;
        assert(paramHandler.getValue(key, f));
        cp->initializeFloat(f);
      } break;

      case ControlParameterValueKind::S64: {
        s64 f;
        assert(paramHandler.getValue(key, f));
        cp->initializeInteger(f);
      } break;

      case ControlParameterValueKind::VEC3_DOUBLE: {
        std::vector<double> vv;
        assert(paramHandler.getVector(key, vv));
        assert(vv.size() == 3);
        Vec3<double> v(vv[0], vv[1], vv[2]);
        cp->initializeVec3d(v);
      } break;

      case ControlParameterValueKind::VEC3_FLOAT: {
        std::vector<float> vv;
        assert(paramHandler.getVector(key, vv));
        assert(vv.size() == 3);
        Vec3<float> v(vv[0], vv[1], vv[2]);
        cp->initializeVec3f(v);
      } break;

      default:
        throw std::runtime_error("can't read type " +
                                 std::to_string((u32)cp->_kind) +
                                 " from yaml file");
        break;
    }
  }
}

/*!
 * Set parameters from a YAML file
 * @param path : the file name
 */
void ControlParameters::initializeFromYamlFile(const std::string& path) {
  ParamHandler paramHandler(path);

  std::vector<std::string> keys = paramHandler.getKeys();

  for (auto& key : keys) {
    // prev handle
    if(key.substr(1,path.length()) != path) continue;
    ControlParameter& cp = collection.lookup(key.erase(0, path.length()+2));
    switch (cp._kind) {
      case ControlParameterValueKind::DOUBLE: {
        double d;
        assert(paramHandler.getValue(key, d));
        cp.initializeDouble(d);
      } break;

      case ControlParameterValueKind::FLOAT: {
        float f;
        assert(paramHandler.getValue(key, f));
        cp.initializeFloat(f);
      } break;

      case ControlParameterValueKind::S64: {
        s64 f;
        assert(paramHandler.getValue(key, f));
        cp.initializeInteger(f);
      } break;

      case ControlParameterValueKind::VEC3_DOUBLE: {
        std::vector<double> vv;
        assert(paramHandler.getVector(key, vv));
        assert(vv.size() == 3);
        Vec3<double> v(vv[0], vv[1], vv[2]);
        cp.initializeVec3d(v);
      } break;

      case ControlParameterValueKind::VEC3_FLOAT: {
        std::vector<float> vv;
        assert(paramHandler.getVector(key, vv));
        assert(vv.size() == 3);
        Vec3<float> v(vv[0], vv[1], vv[2]);
        cp.initializeVec3f(v);
      } break;

      default:
        throw std::runtime_error("can't read type " +
                                 std::to_string((u32)cp._kind) +
                                 " from yaml file");
        break;
    }
  }
}
