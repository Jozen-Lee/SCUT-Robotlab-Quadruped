/**
  ******************************************************************************
  * Copyright (c) 2022 - ~, SCUT-RobotLab Development Team
  * @file    FootStepPlanner.cpp
  * @author  
  * @brief   Code for .
  * @date   2022-02-28
  * @version 1.0
  * @par Change Log:
  * <table>
  * <tr><th>Date        <th>Version  <th>Author     <th>Description
  * </table>
  * 
  ******************************************************************************
  * @attention
  * 
  * if you had modified this file, please make sure your code does not have many 
  * bugs, update the version Number, write dowm your name and the date, the most
  * important is make sure the users will have clear and definite understanding 
  * through your new brief.
  *
  * <h2><center>&copy; Copyright (c) 2022 - ~,SCUT-RobotLab Development Team.
  * All rights reserved.</center></h2>
  ******************************************************************************
  */
#include "common/Math/orientation_tools.h"
#include "common/Controllers/FootStepPlanner.h"

float FootplanCosts::distanceToGoal(FootplanState &state, FootplanGoal &goal) {
  Vec2<float> dp = state.pBase - goal.goalPos;
  return dp.norm();
}

FootstepPlanner::FootstepPlanner(bool verbose) : _verbose(verbose) {
  _stats.reset();
  defaults.trotting = {{true, false, false, true},
                       {false, true, true, false}};
  defaults.standing = {{true, true, true, true}};
}

void FootstepPlanner::reset() {
  _stats.reset();
  _stateCosts.clear();
  _transitionCosts.clear();
}

void FootstepPlanner::buildInputTrajectory(float duration, float dt, InputTrajectoryState x0, float omega) {
  if(_verbose) {
    printf("Input trajectory with %d steps\n", (int)(duration / dt));
  }
  _inputTrajectory.clear();
  _inputTrajectory.reserve(duration / dt);

  Vec3<float> velocity(x0.v[0], x0.v[1], 0.f);
  Vec3<float> position(x0.p[0], x0.p[1], 0.f);
  float theta = x0.theta;
  float t = 0;
  for(uint32_t i = 0; i < (duration / dt); i++) {
    Vec3<float> vRot = ori::coordinateRotation(ori::CoordinateAxis::Z, theta).transpose() * velocity;

    _inputTrajectory.push_back({{position[0], position[1]}, {vRot[0], vRot[1]}, theta});

    position += vRot * dt;
    t += dt;
    theta += omega * dt;
  }
}

void FootstepPlanner::planFixedEvenGait(std::vector<ContactState> &gait, float gait_period) {
  (void)gait;
  (void)gait_period;
}


	/************************ COPYRIGHT(C) SCUT-RobotLab Development Team **************************/


