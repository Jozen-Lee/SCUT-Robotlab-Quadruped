#ifndef _COMMONTYPE_H_
#define _COMMONTYPE_H_

enum LegsName:int {RF, LF, LH, RH};
enum RPY:int {Roll, Pitch, Yaw};
enum Angle:int {Alpha, Beta, Gamma};
enum JointName:int {FrontPart, HindPart, Abduction};


#endif
