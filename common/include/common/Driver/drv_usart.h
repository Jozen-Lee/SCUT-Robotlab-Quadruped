 /**
 * @file rc_usart.h
 * @author LJY (2250017028@qq.com)
 * @brief 
 * @version 0.1
 * @date 2022-03-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
 #ifndef _DRV_USART_H_
 #define _DRV_USART_H_
#include "ros/ros.h"
 #include <serial/serial.h>
 
#define MAX_NUM 300


class Usart
{
public:
    Usart(std::string port_name, 
        std::int32_t baudrate = 115200, 
        std::int16_t out_time = 100){
            // 串口初始化
            serial::Timeout serial_timeout = serial::Timeout::simpleTimeout(out_time); 
            serial_port.setPort(port_name);
            serial_port.setBaudrate(baudrate);
            serial_port.setTimeout(serial_timeout);
    }
    void getData(void* dst, uint16_t size);
    void sendData(void* src, uint16_t size);
    uint8_t updateData(void);
    void start(void);
    void stop(void);

private:
    serial::Serial serial_port;
    uint8_t rev_buf[MAX_NUM];
};
 #endif
