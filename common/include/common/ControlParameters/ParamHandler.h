/**
 * @file ParamsConfig.h
 * @author JozenLee (2250017028@qq.com)
 * @brief  This file is used to load parameters from .yaml files
 * @version 0.1
 * @date 2022-03-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef __PARAMHANDLER_H_
#define __PARAMHANDLER_H_

#include "ros/ros.h"
#include <iostream>

class ParamHandler{
public:
    ParamHandler(const std::string &path): _path(path + "/"){}
    ~ParamHandler() {}
    // template<typename T>
    // T getParam(const std::string& name,const T& defaultValue)
    // {
    //     ros::param::getParamNames
    //     T value;
    //     if(ros::param::get(name,value)){
    //     }
    //     else return defaultValue;//if the parameter haven't been set,it's value will return defaultValue.
    // }
    // template<typename T>
    // T setParam(const std::string& name,const T& value)
    // {
    //     ros::param::set(name, value);
    // }
    bool getString(const std::string &key, std::string& str_value){
        return ros::param::get(_path + key, str_value);
    }
    bool getString(const std::string &category, const std::string &key, std::string & str_value){
        return ros::param::get(_path + category + "/" + key, str_value);
    }

    template<typename T>
    bool getVector(const std::string &key, std::vector<T> &vec_value) {
        return ros::param::get(_path + key, vec_value);
    }

    template<typename T>
    bool getVector(const std::string &category, const std::string &key, std::vector<T> &vec_value) {
        return ros::param::get(_path + category + "/" + key, vec_value);
    }

    template<typename T>
    bool get2DArray(const std::string &category, const std::string &key, std::vector<std::vector<T> > &vec_value) {
        return ros::param::get(_path + category + "/" + key, vec_value);
    }

    template<typename T>
    bool getValue(const std::string &key, T &T_value) {
         return ros::param::get(_path + key, T_value);
    }

    template<typename T>
    bool getValue(const std::string &category, const std::string &key, T &T_value) {
        return ros::param::get(_path + category + "/" + key, T_value);
    }

    bool getBoolean(const std::string & category, const std::string &key, bool &bool_value){
        return ros::param::get(_path + category + "/" + key, bool_value);
    }


    std::vector<std::string> getKeys() {
        std::vector<std::string> v;
        ros::param::getParamNames(v);
        return v;
    }


    bool getBoolean(const std::string &key, bool &bool_value){
        return ros::param::get(_path + key, bool_value);
    }

    bool getInteger(const std::string &key, int &int_value){
        return ros::param::get(_path + key, int_value);
    }
private:
    std::string _path;
};


#endif