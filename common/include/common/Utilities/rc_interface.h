/**
 * @file rt_rc_interface.h
 *
 */
#ifndef _RC_INTERFACE_H
#define _RC_INTERFACE_H

class rc_control_settings {
  public:
    int         mode;       
    float     p_des[2]; // (x, y) -1 ~ 1
    float     height_variation; // -1 ~ 1
    float     v_des[3]; // -1 ~ 1 * (scale 0.5 ~ 1.5)
    float     rpy_des[3]; // -1 ~ 1
    float     omega_des[3]; // -1 ~ 1
    float     variable[3];
    float     test_data[12]; 
    int         test_type[2];
};


namespace RC_mode{
  constexpr int OFF = 0;
  constexpr int QP_STAND = 3;
  constexpr int BACKFLIP_PRE = 4;
  constexpr int BACKFLIP = 5;
  constexpr int VISION = 6;
  constexpr int LOCOMOTION = 11;
  constexpr int RECOVERY_STAND = 12;

/* modes used to test */
  constexpr int STAND_UP = 13;
  constexpr int PD_JOINT = 14;
  constexpr int FRONT_JUMP= 15;
  constexpr int JUMP= 16;

  // Experiment Mode
  constexpr int TWO_LEG_STANCE_PRE = 20;
  constexpr int TWO_LEG_STANCE = 21;

  // Joint Test Mode
};

void sbus_packet_complete();

void get_rc_control_settings(void* settings);
//void get_rc_channels(void* settings);

void* v_memcpy(void* dest, volatile void* src, size_t n);

float deadband(float command, float deadbandRegion, float minVal, float maxVal);

#endif
