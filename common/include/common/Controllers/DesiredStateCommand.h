/**
  ******************************************************************************
  * Copyright (c) 2022 - ~, SCUT-RobotLab Development Team
  * @file    DesiredStateCommand.h
  * @author   
  * @brief   
  ******************************************************************************
  */
#ifndef DESIREDSTATECOMMAND_H
#define DESIREDSTATECOMMAND_H

/* Includes ------------------------------------------------------------------*/
#include <iostream>
#include "common/cppTypes.h"
#include "common/Utilities/rc_interface.h"
#include "common/ControlParameters/RobotParameters.h"
#include "common/Controllers/StateEstimatorContainer.h"
#include "common/cppTypes.h"
/* Private macros ------------------------------------------------------------*/
/* Private type --------------------------------------------------------------*/
enum cmdDes{
  X_position = 0,
  Y_position ,
  Z_position , // Height
  Roll,
  Pitch,
  Yaw,
  X_vel, // forward linear velocity
  Y_vel, // lateral linear velocity
  Z_vel, // vertical linear velocity
  Roll_rate,     
  Pitch_rate,  
  Yaw_rate
};
/* Exported function declarations --------------------------------------------*/	

/**
 *
 */
template <typename T>
struct DesiredStateData {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  DesiredStateData() { zero(); }

  // Zero out all of the data
  void zero();

  // Instantaneous desired state command
  Vec12<T> stateDes;

  Vec12<T> pre_stateDes;

  // Desired future state trajectory (for up to 10 timestep MPC)
  Eigen::Matrix<T, 12, 10> stateTrajDes;
};

/**
 *
 */
template <typename T>
class DesiredStateCommand {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  // Initialize with the GamepadCommand struct
  DesiredStateCommand(rc_control_settings* rc_command,
                      RobotControlParameters* _parameters,
                      StateEstimate<T>* sEstimate,  float _dt) {
    rcCommand = rc_command;
    parameters = _parameters;
    stateEstimate = sEstimate;

    data.stateDes.setZero();
    data.pre_stateDes.setZero();
    leftAnalogStick.setZero();
    rightAnalogStick.setZero();

    dt = _dt;
  }

  void convertToStateCommands();
  void setCommandLimits(T minVelX_in, T maxVelX_in,
                        T minVelY_in, T maxVelY_in, T minTurnRate_in, T maxTurnRate_in);
  void desiredStateTrajectory(int N, Vec10<T> dtVec);
  void printRawInfo();
  void printStateCommandInfo();
  float deadband(float command, T minVal, T maxVal);

  // These should come from the inferface
  T maxRoll = 0.4;
  T minRoll = -0.4;
  T maxPitch = 0.4;
  T minPitch = -0.4;
  T maxVelX = 3.0;
  T minVelX = -3.0;
  T maxVelY = 2.0;
  T minVelY = -2.0;
  T maxTurnRate = 2.5;
  T minTurnRate = -2.5;

  Vec2<float> leftAnalogStick;
  Vec2<float> rightAnalogStick;

  // Holds the instantaneous desired state and future desired state trajectory
  DesiredStateData<T> data;

  const rc_control_settings* rcCommand;

  bool trigger_pressed = false;

private:
  StateEstimate<T>* stateEstimate;
  RobotControlParameters* parameters;

  // Dynamics matrix for discrete time approximation
  Mat12<T> A;

  // Control loop timestep change
  T dt;

  // Value cutoff for the analog stick deadband
  T deadbandRegion = 0.075;

  //const T filter = 0.01;
  const T filter = 0.1;

  // Choose how often to print info, every N iterations
  int printNum = 5;  // N*(0.001s) in simulation time

  // Track the number of iterations since last info print
  int printIter = 0;
};

#endif
/************************ COPYRIGHT(C) SCUT-ROBOTLAB **************************/
 

