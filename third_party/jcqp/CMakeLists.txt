cmake_minimum_required(VERSION 3.0.2)
project(jcqp)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
)

catkin_package(
    INCLUDE_DIRS include
  #  LIBRARIES jcqp
  # CATKIN_DEPENDS roscpp
  #  DEPENDS system_lib
  )

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include 
  include/jcqp
  ${catkin_INCLUDE_DIRS}
  )
include_directories(amd/include)

file(GLOB_RECURSE sources "src/*.cpp")      
add_library(jcqp 
        src/QpProblem.cpp
        src/ProblemGenerator.cpp
        src/eigenvalues.cpp
        src/CholeskyDenseSolver.cpp
        src/CholeskySparseSolver.cpp
        src/SparseMatrixMath.cpp
        amd/src/amd_1.c
        amd/src/amd_2.c
        amd/src/amd_aat.c
        amd/src/amd_control.c
        amd/src/amd_defaults.c
        amd/src/amd_info.c
        amd/src/amd_order.c
        amd/src/amd_post_tree.c
        amd/src/amd_postorder.c
        amd/src/amd_preprocess.c
        amd/src/amd_valid.c
        amd/src/SuiteSparse_config.c)

###########
## Build ##
###########
target_link_libraries(jcqp pthread)



