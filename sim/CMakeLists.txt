cmake_minimum_required(VERSION 3.0.2)
project(sim)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages

find_package(catkin REQUIRED roscpp std_msgs sensor_msgs common)

catkin_package(
# INCLUDE_DIRS include
#  LIBRARIES webots_ctrler
#  CATKIN_DEPENDS message_runtime
#  DEPENDS system_lib
)

if(NOT CMAKE_CONFIGURATION_TYPES AND NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

link_directories($ENV{WEBOTS_HOME}/lib/controller)

set (LIBRARIES ${CMAKE_SHARED_LIBRARY_PREFIX}Controller${CMAKE_SHARED_LIBRARY_SUFFIX} 
               ${CMAKE_SHARED_LIBRARY_PREFIX}CppController${CMAKE_SHARED_LIBRARY_SUFFIX} 
)

set(EXECUTABLE_OUTPUT_PATH ../)
               
include_directories($ENV{WEBOTS_HOME}/include/controller/c $ENV{WEBOTS_HOME}/include/controller/cpp)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  $ENV{WEBOTS_HOME}/include/controller/cpp
  $ENV{WEBOTS_HOME}/include/controller/c
)

add_executable(webots_controller
  controllers/webots_controller/webots_controller.cpp
)

target_link_libraries(webots_controller
  ${catkin_LIBRARIES}
  ${LIBRARIES}
  common
)

SET(CMAKE_BUILD_TYPE Debug) 

