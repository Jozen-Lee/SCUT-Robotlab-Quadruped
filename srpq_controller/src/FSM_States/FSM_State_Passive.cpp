/**
 * @file FSM_State_Passive.cpp
 * @author jozenlee(2250017028@qq.com)
 * @brief
 * @version 0.1
 * @date 2022-03-09
 *
 * @copyright Copyright (c) 2022
 *
 */

/*============================== Passive ==============================*/
/**
 * FSM State that calls no controls. Meant to be a safe state where the
 * robot should not do anything as all commands will be set to 0.
 */

#include "FSM_State_Passive.h"

/**
 * Constructor for the FSM State that passes in state specific info to
 * the generic FSM State constructor.
 *
 * @param _controlFSMData holds all of the relevant control data
 */
template <typename T>
FSM_State_Passive<T>::FSM_State_Passive(ControlFSMData<T>* _controlFSMData)
    : FSM_State<T>(_controlFSMData, FSM_StateName::PASSIVE, "PASSIVE") {
    // Do nothing
    // Set the pre controls safety checks
    this->checkSafeOrientation = false;

    // Post control safety checks
    this->checkPDesFoot = false;
    this->checkForceFeedForward = false;
}

/**
 *功能：进入状态时要执行的行为
 */
template <typename T>
void FSM_State_Passive<T>::onEnter() {
    // Default is to not transition
    this->nextStateName = this->stateName;

    // Reset the transition data
    this->transitionData.zero();
}

/**
 * 实现父类虚函数的具体功能
 * Calls the functions to be executed on each control loop iteration.
 */
template <typename T>
void FSM_State_Passive<T>::run() {
    // Do nothing, all commands should begin as zeros
    testTransition();
}

/**
 * 功能：处理机器人在状态之间的实际转换。在转换完成时返回true。
 * Handles the actual transition for the robot between states.
 * Returns true when the transition is completed.
 *
 * @return true if transition is complete
 */
template <typename T>
TransitionData<T> FSM_State_Passive<T>::testTransition() {
    this->transitionData.done = true;
    return this->transitionData;
}

/**
 * 功能：状态转移检查：管理哪些状态可以由用户命令或状态事件触发器切换
 * Manages which states can be transitioned into either by the user
 * commands or state event triggers.
 *
 * @return the enumerated FSM state name to transition into
 */
template <typename T>
FSM_StateName FSM_State_Passive<T>::checkTransition() {
    this->nextStateName = this->stateName;
    iter++;

    // Switch FSM control mode
    switch ((int)this->_data->controlParameters->control_mode) {
        case K_PASSIVE:    // normal c (0)
            // Normal operation for state based transitions
            break;

        case K_JOINT_PD:
            // Requested switch to joint PD control
            this->nextStateName = FSM_StateName::JOINT_PD;
            break;

        case K_STAND_UP:
            // Requested switch to joint PD control
            this->nextStateName = FSM_StateName::STAND_UP;
            break;

        case K_RECOVERY_STAND:
            // Requested switch to joint PD control
            this->nextStateName = FSM_StateName::RECOVERY_STAND;
            break;

        case K_LOCOMOTION:
            // Requested switch to joint PD control
            this->nextStateName = FSM_StateName::LOCOMOTION;
            break;

            case K_FRONTJUMP:
            // Requested switch to joint PD control
            this->nextStateName = FSM_StateName::FRONTJUMP;
            break;

            case K_JUMP:
            // Requested switch to joint PD control
            this->nextStateName = FSM_StateName::JUMP;
            break;

        default:
            std::cout << "[CONTROL FSM] Bad Request: Cannot transition from " << K_PASSIVE << " to "
                      << this->_data->controlParameters->control_mode << std::endl;
    }

    // Get the next state
    return this->nextStateName;
}

/**
 * 功能：状态转移， 处理机器人在状态之间的实际转换
 * Handles the actual transition for the robot between states.
 * Returns true when the transition is completed.
 *
 * @return true if transition is complete
 */
template <typename T>
TransitionData<T> FSM_State_Passive<T>::transition() {
    // Finish Transition
    this->transitionData.done = true;

    // Return the transition data to the FSM
    return this->transitionData;
}

/**
 * 在退出状态时清除状态信息。
 * Cleans up the state information on exiting the state.
 */
template <typename T>
void FSM_State_Passive<T>::onExit() {
    // Nothing to clean up when exiting
}

// template class FSM_State_Passive<double>;
template class FSM_State_Passive<float>;
