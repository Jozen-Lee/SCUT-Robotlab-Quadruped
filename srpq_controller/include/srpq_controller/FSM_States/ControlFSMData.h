#ifndef CONTROLFSMDATA_H
#define CONTROLFSMDATA_H

#include <iostream>
#include "common/ControlParameters/RobotParameters.h"
#include "common/ControlParameters/UserParameters.h"
#include "common/Controllers/DesiredStateCommand.h"
#include "common/Controllers/GaitScheduler.h"
#include "common/Controllers/LegController.h"
#include "common/Controllers/StateEstimatorContainer.h"
#include "common/Dynamics/Quadruped.h"


/**
 *
 */
template <typename T>
struct ControlFSMData {
  // EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Quadruped<T>* _quadruped;
  LegController<T>* _legController;
  GaitScheduler<T>* _gaitScheduler;
  DesiredStateCommand<T>* _desiredStateCommand;
  RobotControlParameters* controlParameters;
  UserParameters* userParameters;
  StateEstimatorContainer<T>* _stateEstimator;
};

template struct ControlFSMData<double>;
template struct ControlFSMData<float>;

#endif  // CONTROLFSM_H