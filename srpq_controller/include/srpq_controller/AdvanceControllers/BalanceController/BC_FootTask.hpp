#ifndef BC_FOOT_TASK_H
#define BC_FOOT_TASK_H

#include "srpq_controller/AdvanceControllers/BalanceController/BalanceCtrlTask.hpp"

template <typename T>
class FootTask : public BalanceCtrlTask<T> {
public:
    FootTask(){}
    ~FootTask(){}
private:
    void  task(ControlFSMData<T>& data, BalanceCtrlData<T>* ctrl_data, BCoutputData<T>* BCout, int leg);
};

#endif
