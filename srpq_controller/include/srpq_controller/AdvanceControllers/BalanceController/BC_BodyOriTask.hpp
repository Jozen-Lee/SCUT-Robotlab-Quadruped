#ifndef BC_BODYORI_TASK_H
#define BC_BODYORI_TASK_H

#include "srpq_controller/AdvanceControllers/BalanceController/BalanceCtrlTask.hpp"

template <typename T>
class BodyOriTask : public BalanceCtrlTask<T> {
public:
    BodyOriTask(){}
    ~BodyOriTask(){}
private:
    void  task(ControlFSMData<T>& data, BalanceCtrlData<T>* ctrl_data, BCoutputData<T>* BCout, int leg);
    bool updateFlag = false;
    Vec3<T> posture_world_des;
};

#endif
