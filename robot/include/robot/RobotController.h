/**
  ******************************************************************************
  * Copyright (c) 2022 - ~, SCUT-RobotLab Development Team
  * @file    Hardware_Node.h
  * @author   
  * @brief   
  ******************************************************************************
  */
#ifndef ROBOTCONTROLLER_H
#define ROBOTCONTROLLER_H

/* Includes ------------------------------------------------------------------*/
#include "common/Controllers/DesiredStateCommand.h"
#include "common/ControlParameters/RobotParameters.h"
#include "common/Controllers/LegController.h"
#include "common/Dynamics/Quadruped.h"

/* Private macros ------------------------------------------------------------*/
/* Private type --------------------------------------------------------------*/
/* Exported function declarations --------------------------------------------*/	

/*!
 * Parent class of user robot controllers
 */
class RobotController{
  friend class RobotRunner;
public:
  RobotController(){}
  virtual ~RobotController(){}

  virtual void initializeController() = 0;
/**
 * Called one time every control loop 
 */
  virtual void runController() = 0;
  // virtual void updateVisualization() = 0;
  // virtual ControlParameters* getUserControlParameters() = 0;
  virtual void Estop() {}

protected:
  Quadruped<float>* _quadruped = nullptr;
  FloatingBaseModel<float>* _model = nullptr;
  RobotControlParameters* _controlParameters = nullptr;
  StateEstimatorContainer<float>* _stateEstimator = nullptr;
  StateEstimate<float>* _stateEstimate = nullptr;
  LegController<float>* _legController = nullptr;
  DesiredStateCommand<float>* _desiredStateCommand = nullptr;
  
};

#endif
/************************ COPYRIGHT(C) SCUT-ROBOTLAB **************************/
 

 

