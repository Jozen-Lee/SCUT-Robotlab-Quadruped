/**
  ******************************************************************************
  * Copyright (c) 2022 - ~, SCUT-RobotLab Development Team
  * @file    QT_Node.h
  * @author   
  * @brief   
  ******************************************************************************
  */
#ifndef QT_NODE_H
#define QT_NODE_H

/* Includes ------------------------------------------------------------------*/
#include "ros/ros.h"
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/JointState.h>
#include "common/RCCommand.h"
#include "common/DataType/RobotCtrlType.h"

/* Private macros ------------------------------------------------------------*/

/* Private type --------------------------------------------------------------*/
/* Exported function declarations --------------------------------------------*/	
class QT_Node{
public:
  QT_Node(ros::NodeHandle* _n):n(_n){ Node_Init();}
  ~QT_Node(){}
  
  void Run(void);

private:
  ros::NodeHandle* n;
  ros::Subscriber  RobotStatus_Suber;
  ros::Subscriber  FeetCmd_Suber;
  ros::Subscriber  JointsCmd_Suber;
  ros::Subscriber  Joystick_Suber;
  ros::Publisher     RC_Puber;
  common::RCCommand  rc_cmd;
  // void RobotStatus_Callback(const ros_connection::Robot& msg);
  void Node_Init(void);
  void FeetCmd_Callback(const sensor_msgs::JointStateConstPtr& msg);
  void JointsCmd_Callback(const sensor_msgs::JointStateConstPtr& msg);
  void JoystickCmd_Callback(const sensor_msgs::JointStateConstPtr& msg);
};

#endif
/************************ COPYRIGHT(C) SCUT-ROBOTLAB **************************/
 

