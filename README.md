 # SCUT Robotlab Quadruped

[![standard-readme compliant](https://img.shields.io/badge/version-2.0-blue.svg?style=flat-square)](https://gitee.com/Jozen-Lee/quadruped_-project)

[![standard-readme compliant](https://img.shields.io/badge/developper-JozenLee%20-brightgreen.svg?style=flat-square)](https://gitee.com/Jozen-Lee/quadruped_-project)

**该项目由华南理工大学机器人实验室项目组进行开发和维护，有相关开发经验或者开发中遇到问题的友友欢迎与我进行联系交流：2250017028@qq.com**



# 1. 代码架构

这个项目是基于MIT的Cheetah开源项目进行开发设计的，引入了ROS通信网络，并且控制上进行了简化。所以项目文件中有一部分代码文件并没有调用，下面列举的是作品使用到的，比较核心的代码模块，以供参考。

## common 

### Utilities 		

通用的算法，函数

### Math

常用的数学公式，如旋转矩阵/欧拉角/四元数的相互转换。

### DataType

通用的数据结构体定义

### Controllers

**DesiredStateCommand**：遥控器数据解包转为控制指令

**FootSwingTrajectory**：贝塞尔曲线的代码实现

**GaitScheduler**：步态规划器

**LegController**：腿部控制器，包括数据更新以及输出控制指令

**OrientationEstimator**：姿态估计

**PositionVelocityEstimator**：位置速度估计

### msg

ROS网络中的自定义消息



## config

> 包含了项目所有的参数文件，采用**yaml文件进行参数存储**，并使用**rosparam**进行参数加载。

**robot_parameters.yaml**：机器人自身的相关属性参数

**user_parameters.yaml**：用户人为调节的算法参数



## robot	

机器人整体运行逻辑，主要包括了机器人**启动时的数据初始化**，以及制定**运行过程中的各个算法执行顺序**和**数据流向**。



## sim		

> webots仿真环境的搭建和驱动相关文件	

webots_controller：基于ROS的webots控制器，用于**控制webots中机器人的各个外设的工作**，以及获取**各个外设和仿真环境的相关数据**。



## srpq_controller	

**FSM_States**：状态机相关代码（状态包括有运动态，站立态，被动态等，与步态的概念不同）

**BalanceController**：平衡控制器，模仿了WBC多任务，分优先级的思想。

**SportsController**：运动控制器，采用纯位控的形式进行机器人控制，也是目前实体控制唯一验证通过的算法。

**convexMPC**：MPC控制器

**WBC/WBC_Ctrl**：WBC控制器



## ros_network	

> ROS通信网络，由于时间有限，本项目仅构建了三个节点，并且将所有算法在一个节点上运行，整体架构不太优雅，建议读者可以基于该架构进行重新设计一套ROS通信网络。

**Control_Node**：核心的逻辑控制节点，主要负责算法运行，以及数据整合

**Hardware_Node**：硬件通信节点，主要负责与底层主控STM32进行通信。

**QT_Node**：上位机通信节点



## third_party	

第三方库



# 2. 依赖项安装

**qpOASES二次优化库**

```
cd third_party/qpOASES
mkdir build
cd build
cmake ..
sudo make install
```



# 3. 代码运行

首先需要将该项目的所有文件导入ROS工作空间中，建议**新建一个工作空间**。

1. 创建并初始化工作空间

   ```
   mkdir -p ~/catkin_dog/src
   cd ~/catkin_dog/src
   catkin_init_workspace
   ```

2. 编译工作空间，并生成安装空间

   ```
   cd ~/catkin_dog/
   catkin_make
   catkin_make install
   ```

3. 用本项目文件覆盖掉**catkin_dog/src**目录下的文件

4. 编译

   需要**先编译common功能包**（里面有msg文件，其他文件会调用），如果报错也可以**多编译几次**

   ```
   cd ~/catkin_dog/
   catkin_make
   ```

5. 将工作空间地址加入**.bashrc**文件中

**运行仿真环境**

```
roslaunch ros_network bring_up.launch 
```

**运行实体控制**

```
roslaunch ros_network bring_up.launch sim:=false
```

